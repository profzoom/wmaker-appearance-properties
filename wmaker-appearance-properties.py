#! /usr/bin/python
from gi.repository import Gtk,Gdk
from gi.repository.GdkPixbuf import Pixbuf
from subprocess import call, check_output
import glob, os, imghdr, shutil, shlex

def toSixDigit(color):
    red = int(color.red/256)
    green = int(color.green/256)
    blue = int(color.blue/256)
    return '#' + str(hex(65536*red + 256*green + blue))[2:].zfill(6)

def changeBackground():
    image = bgListstore[iconview.get_selected_items()[0]][1]
    colorStyle = color_combobox.get_active_text()
    color1 = toSixDigit(colorbutton1.get_color())
    color2 = toSixDigit(colorbutton2.get_color())
    if image == "none":
        if colorStyle == "Solid color":
            texture = "(solid, \"" + color1 + "\")"
        else:
            texture = "(" + colorStyle[0].lower() + "gradient,\"" + color1 + "\",\"" + color2 + "\")"
        call(["wdwrite","WindowMaker","WorkspaceBack",texture]) 
        print texture
    else:
        imageStyle = combobox.get_active_text().lower()
        call(["wmsetbg","--"+imageStyle,"-u",image,"-b","\"" + color1 + "\""])
        print imageStyle + " " + image + " " + color1
    return

def imageSelect(arg):
    image = bgListstore[iconview.get_selected_items()[0]][1]
    if image == "none":
        colorComboBoxEntries(["Solid color","Diagonal gradient","Horizontal gradient","Vertical gradient"])
        remove_button.set_sensitive(False)
        combobox.set_sensitive(False)
    else:
        colorComboBoxEntries(["Solid color"])
        if os.path.dirname(image) == os.environ.get("GNUSTEP_USER_ROOT") + "/Library/WindowMaker/Backgrounds":
            remove_button.set_sensitive(True)
        else:
            remove_button.set_sensitive(False)
        combobox.set_sensitive(True)
    if favorite_texture == "solid":
        color_combobox.set_active(0)
    if favorite_texture == "dgradient":
        color_combobox.set_active(1)
    if favorite_texture == "hgradient":
        color_combobox.set_active(2)
    if favorite_texture == "vgradient":
        color_combobox.set_active(3)
    return


def styleSelect(arg):
    changeBackground()

def colorSelect(arg):
    style = color_combobox.get_active_text()
    if "gradient" in style:
        colorbutton2.set_visible(True)
    else:
        colorbutton2.set_visible(False)
    changeBackground()

def colorbuttonSelect(arg):
    changeBackground()

def removeImage(arg):
    image = bgListstore[iconview.get_selected_items()[0]][1]
    image_pos = 0
    while bgListstore[image_pos][1] != image:
       image_pos += 1
    os.remove(image)
    refreshImages()
    path = Gtk.TreePath.new_from_string(str(image_pos))
    if len(bgListstore) == image_pos:
        selectImage("none")
    else:
        iconview.select_path(path)
    return

def addImage(arg):
    dialog = Gtk.FileChooserDialog("Add Wallpaper",window,Gtk.FileChooserAction.OPEN,(Gtk.STOCK_CANCEL,Gtk.ResponseType.CANCEL,Gtk.STOCK_OPEN,Gtk.ResponseType.OK))
    image_filter = Gtk.FileFilter()
    image_filter.add_pixbuf_formats()
    image_filter.set_name("Images")
    dialog.add_filter(image_filter)
    all_filter = Gtk.FileFilter()
    all_filter.add_pattern('*')
    all_filter.set_name("All files")
    dialog.add_filter(all_filter)
    response = dialog.run()
    if response == Gtk.ResponseType.OK:
        new_location = os.environ.get("GNUSTEP_USER_ROOT") + "/Library/WindowMaker/Backgrounds/" + os.path.basename(dialog.get_filename())
        shutil.copyfile(dialog.get_filename(),new_location)
        refreshImages()
        selectImage(new_location)
    dialog.destroy()
    return

def selectImage(image):
    i = 0
    while bgListstore[i][1] != image:
       i += 1
    path = Gtk.TreePath.new_from_string(str(i))
    iconview.select_path(path)
    return



def refreshImages():
    bgListstore.clear()

    # no image option
    bgListstore.append([None,"none","No image"])



    backgrounds = []
    for directory in directories:
        backgrounds += glob.glob(os.path.expanduser(directory + '/*'))

    if initial_pixmap != None:
        if os.path.dirname(initial_pixmap) != "" and initial_pixmap not in backgrounds:
            backgrounds += [initial_pixmap]

    for background in backgrounds:
        try:
            filename = os.path.basename(background)
            header = os.path.splitext(filename)[1][1:]
            if header in fileExtensions:
                pixbuf = Pixbuf.new_from_file(background)
                width = Pixbuf.get_width(pixbuf)
                height = Pixbuf.get_height(pixbuf)
                pixbuf = Pixbuf.new_from_file_at_size(background,min(width,128),min(height,128)) #rescales to 128 x 128
                tooltip = filename + "\n" 
                tooltip += header.upper() + " image, " + str(width) + " pixels by " + str(height) + " pixels\n"
                tooltip += "Folder:  " + os.path.dirname(background)
                bgListstore.append([pixbuf,background,tooltip])
        except IOError: #if file is actually a directory
            pass
    return


def colorComboBoxEntries(entries):
    comboboxEntries = Gtk.ListStore(str)
    for entry in entries:
        comboboxEntries.append([entry])
    color_combobox.set_model(comboboxEntries)
    return

def parsePropertyList(stringToParse):
    parsedString = shlex.shlex(stringToParse,posix=True)
    parsedString.whitespace += '(,)'
    parsedString.whitespace_split = True
    return list(parsedString)


builder = Gtk.Builder()
builder.add_from_file("wmaker-appearance-properties.glade")
handlers = {
    "gtk_main_quit": Gtk.main_quit,
    "image_select":  imageSelect,
    "style_select":  styleSelect,
    "color_select":  colorSelect,
    "colorbutton_select":  colorbuttonSelect,
    "remove_image":  removeImage,
    "add_image":  addImage,
}
builder.connect_signals(handlers)

#list of background images
bgListstore = Gtk.ListStore(Pixbuf,str,str)
iconview = builder.get_object("iconview1")
scrolledwindow = builder.get_object("scrolledwindow1")
add_button = builder.get_object("add_button")

remove_button = builder.get_object("remove_button")



directories = parsePropertyList(check_output(["wdread","WindowMaker","PixmapPath"]))

fileExtensions= []
for fileFormat in Pixbuf.get_formats():
    fileExtensions += fileFormat.get_extensions()

    






combobox = builder.get_object("comboboxtext1")

color_combobox = builder.get_object("color_combobox")
colorbutton1 = builder.get_object("colorbutton1")
colorbutton2 = builder.get_object("colorbutton2")


#get initial state
initial_state = parsePropertyList(check_output(["wdread","WindowMaker","WorkspaceBack"]))


if "pixmap" in initial_state[0]:
    initial_pixmap = initial_state[1]
else:
    initial_pixmap = None

refreshImages()



iconview.set_model(bgListstore)
iconview.set_pixbuf_column(0)
iconview.set_tooltip_column(2)

if not "pixmap" in initial_state[0]:
    combobox.set_active(0)
    color1 = Gdk.color_parse(initial_state[1])
    colorbutton1.set_color(color1)
    favorite_texture = initial_state[0]
    if initial_state[0] != "solid":
        color2 = Gdk.color_parse(initial_state[2])
        colorbutton2.set_color(color2)
    selectImage("none")
else:
    color = Gdk.color_parse(initial_state[2])
    favorite_texture = "solid"
    colorbutton1.set_color(color)
    if initial_state[0] == "tpixmap":
        combobox.set_active(0)
    if initial_state[0] == "cpixmap":
        combobox.set_active(1)
    if initial_state[0] == "spixmap":
        combobox.set_active(2)
    if initial_state[0] == "mpixmap":
        combobox.set_active(3)
    filename = initial_state[1]
    if not os.path.exists(filename):
        directory = (directory for directory in directories if os.path.exists(os.path.expanduser(directory) + "/" + filename)).next()
        filename = os.path.expanduser(os.path.normpath(directory + "/" + filename))
    selectImage(filename)

 
      
window = builder.get_object("window1")
window.connect("delete-event", Gtk.main_quit)
window.show_all()

Gtk.main()
