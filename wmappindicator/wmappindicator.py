#! /usr/bin/python

from gi.repository import Gtk,Gdk,GdkX11
from Xlib import display, xobject

builder = Gtk.Builder()
builder.add_from_file("wmappindicator.glade")
dockapp = builder.get_object("dockapp")
dockapp.connect("delete-event", Gtk.main_quit)

dockapp.set_wmclass("foo","Dockapp")
dockapp.show()

gdkDockapp = dockapp.get_window()
xDockapp = gdkDockapp.get_xid()

gdkDisplay = gdkDockapp.get_display()
xDisplay = gdkDisplay.get_xdisplay()

xDockapp.get_wm_hints()

Gtk.main()

